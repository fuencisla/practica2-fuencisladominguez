package Ventanas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JToolBar;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JMenuItem;
import java.awt.Color;
import javax.swing.JTabbedPane;
import javax.swing.JList;
import javax.swing.JSpinner;
import javax.swing.JFormattedTextField;
import javax.swing.JComboBox;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import javax.swing.JRadioButton;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JLabel;
import java.awt.SystemColor;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;

public class DatosPersonales extends JFrame {

	private JPanel contentPane;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField txtEs;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField;
	private JTextField textField_7;
	private JTextField textField_8;
	private JTextField textField_9;
	private JTextField textField_10;
	private JTextField textField_11;
	private JTextField textField_12;
	private JTextField textField_13;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DatosPersonales frame = new DatosPersonales();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DatosPersonales() {
		setTitle("Fintalla S.L.");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 419);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnDatosPersonales = new JMenu("Datos Personales");
		menuBar.add(mnDatosPersonales);
		
		JMenuItem mntmVerDatos = new JMenuItem("Ver datos");
		mnDatosPersonales.add(mntmVerDatos);
		
		JMenuItem mntmCambiarDatos = new JMenuItem("Cambiar datos");
		mnDatosPersonales.add(mntmCambiarDatos);
		
		JMenuItem mntmEliminarDatos = new JMenuItem("Eliminar datos");
		mnDatosPersonales.add(mntmEliminarDatos);
		
		JMenu mnListaDeLa = new JMenu("Lista de la compra");
		menuBar.add(mnListaDeLa);
		
		JMenu mnSeleccionarTienda = new JMenu("Seleccionar Tienda");
		menuBar.add(mnSeleccionarTienda);
		
		JMenuItem mntmVerTienda = new JMenuItem("Ver tienda");
		mnSeleccionarTienda.add(mntmVerTienda);
		
		JMenuItem mntmSeleccionarTienda = new JMenuItem("Seleccionar tienda");
		mnSeleccionarTienda.add(mntmSeleccionarTienda);
		
		JMenu mnSalir = new JMenu("Salir");
		menuBar.add(mnSalir);
		
		JMenuItem mntmCambiarUsuario = new JMenuItem("Cambiar usuario");
		mnSalir.add(mntmCambiarUsuario);
		
		JMenuItem mntmSalirDeLa = new JMenuItem("Salir de la app");
		mnSalir.add(mntmSalirDeLa);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 228, 181));
		contentPane.setForeground(new Color(0, 0, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		panel.setBackground(SystemColor.controlHighlight);
		tabbedPane.addTab("Datos personales", null, panel, null);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{1.0, 1.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"DNI", "NIE", "PASAPORTE"}));
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox.gridx = 0;
		gbc_comboBox.gridy = 0;
		panel.add(comboBox, gbc_comboBox);
		
		textField = new JTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.gridwidth = 4;
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 1;
		gbc_textField.gridy = 0;
		panel.add(textField, gbc_textField);
		textField.setColumns(10);
		
		JLabel lblNombre = new JLabel("Nombre");
		GridBagConstraints gbc_lblNombre = new GridBagConstraints();
		gbc_lblNombre.anchor = GridBagConstraints.WEST;
		gbc_lblNombre.insets = new Insets(0, 0, 5, 5);
		gbc_lblNombre.gridx = 0;
		gbc_lblNombre.gridy = 1;
		panel.add(lblNombre, gbc_lblNombre);
		
		textField_7 = new JTextField();
		GridBagConstraints gbc_textField_7 = new GridBagConstraints();
		gbc_textField_7.gridwidth = 4;
		gbc_textField_7.insets = new Insets(0, 0, 5, 5);
		gbc_textField_7.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_7.gridx = 1;
		gbc_textField_7.gridy = 1;
		panel.add(textField_7, gbc_textField_7);
		textField_7.setColumns(10);
		
		JLabel lblApellidos = new JLabel("Apellidos");
		GridBagConstraints gbc_lblApellidos = new GridBagConstraints();
		gbc_lblApellidos.anchor = GridBagConstraints.WEST;
		gbc_lblApellidos.insets = new Insets(0, 0, 5, 5);
		gbc_lblApellidos.gridx = 0;
		gbc_lblApellidos.gridy = 2;
		panel.add(lblApellidos, gbc_lblApellidos);
		
		textField_8 = new JTextField();
		GridBagConstraints gbc_textField_8 = new GridBagConstraints();
		gbc_textField_8.gridwidth = 4;
		gbc_textField_8.insets = new Insets(0, 0, 5, 5);
		gbc_textField_8.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_8.gridx = 1;
		gbc_textField_8.gridy = 2;
		panel.add(textField_8, gbc_textField_8);
		textField_8.setColumns(10);
		
		JLabel lblDireccin = new JLabel("Direcci\u00F3n");
		GridBagConstraints gbc_lblDireccin = new GridBagConstraints();
		gbc_lblDireccin.anchor = GridBagConstraints.WEST;
		gbc_lblDireccin.insets = new Insets(0, 0, 5, 5);
		gbc_lblDireccin.gridx = 0;
		gbc_lblDireccin.gridy = 3;
		panel.add(lblDireccin, gbc_lblDireccin);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setModel(new DefaultComboBoxModel(new String[] {"CALLE", "AVENIDA", "TRAVES\u00CDA", "CAMINO", "CARRETERA", "PLAZA", "PASEO"}));
		GridBagConstraints gbc_comboBox_1 = new GridBagConstraints();
		gbc_comboBox_1.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox_1.gridx = 0;
		gbc_comboBox_1.gridy = 4;
		panel.add(comboBox_1, gbc_comboBox_1);
		
		textField_9 = new JTextField();
		GridBagConstraints gbc_textField_9 = new GridBagConstraints();
		gbc_textField_9.gridwidth = 5;
		gbc_textField_9.insets = new Insets(0, 0, 5, 0);
		gbc_textField_9.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_9.gridx = 1;
		gbc_textField_9.gridy = 4;
		panel.add(textField_9, gbc_textField_9);
		textField_9.setColumns(10);
		
		JLabel lblN = new JLabel("N\u00BA");
		GridBagConstraints gbc_lblN = new GridBagConstraints();
		gbc_lblN.anchor = GridBagConstraints.WEST;
		gbc_lblN.insets = new Insets(0, 0, 5, 5);
		gbc_lblN.gridx = 0;
		gbc_lblN.gridy = 5;
		panel.add(lblN, gbc_lblN);
		
		textField_11 = new JTextField();
		GridBagConstraints gbc_textField_11 = new GridBagConstraints();
		gbc_textField_11.insets = new Insets(0, 0, 5, 5);
		gbc_textField_11.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_11.gridx = 1;
		gbc_textField_11.gridy = 5;
		panel.add(textField_11, gbc_textField_11);
		textField_11.setColumns(10);
		
		JLabel lblPiso = new JLabel("Piso");
		GridBagConstraints gbc_lblPiso = new GridBagConstraints();
		gbc_lblPiso.anchor = GridBagConstraints.WEST;
		gbc_lblPiso.gridwidth = 2;
		gbc_lblPiso.insets = new Insets(0, 0, 5, 5);
		gbc_lblPiso.gridx = 2;
		gbc_lblPiso.gridy = 5;
		panel.add(lblPiso, gbc_lblPiso);
		
		textField_10 = new JTextField();
		GridBagConstraints gbc_textField_10 = new GridBagConstraints();
		gbc_textField_10.gridwidth = 2;
		gbc_textField_10.insets = new Insets(0, 0, 5, 0);
		gbc_textField_10.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_10.gridx = 4;
		gbc_textField_10.gridy = 5;
		panel.add(textField_10, gbc_textField_10);
		textField_10.setColumns(10);
		
		JLabel lblCdigoPostal = new JLabel("C\u00F3digo Postal");
		GridBagConstraints gbc_lblCdigoPostal = new GridBagConstraints();
		gbc_lblCdigoPostal.insets = new Insets(0, 0, 5, 5);
		gbc_lblCdigoPostal.anchor = GridBagConstraints.WEST;
		gbc_lblCdigoPostal.gridx = 0;
		gbc_lblCdigoPostal.gridy = 6;
		panel.add(lblCdigoPostal, gbc_lblCdigoPostal);
		
		textField_12 = new JTextField();
		GridBagConstraints gbc_textField_12 = new GridBagConstraints();
		gbc_textField_12.insets = new Insets(0, 0, 5, 5);
		gbc_textField_12.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_12.gridx = 1;
		gbc_textField_12.gridy = 6;
		panel.add(textField_12, gbc_textField_12);
		textField_12.setColumns(10);
		
		JLabel lblCiudad = new JLabel("Localidad");
		GridBagConstraints gbc_lblCiudad = new GridBagConstraints();
		gbc_lblCiudad.anchor = GridBagConstraints.WEST;
		gbc_lblCiudad.gridwidth = 3;
		gbc_lblCiudad.insets = new Insets(0, 0, 5, 5);
		gbc_lblCiudad.gridx = 2;
		gbc_lblCiudad.gridy = 6;
		panel.add(lblCiudad, gbc_lblCiudad);
		
		textField_13 = new JTextField();
		GridBagConstraints gbc_textField_13 = new GridBagConstraints();
		gbc_textField_13.insets = new Insets(0, 0, 5, 0);
		gbc_textField_13.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_13.gridx = 5;
		gbc_textField_13.gridy = 6;
		panel.add(textField_13, gbc_textField_13);
		textField_13.setColumns(10);
		
		JLabel lblProvincia = new JLabel("Provincia");
		GridBagConstraints gbc_lblProvincia = new GridBagConstraints();
		gbc_lblProvincia.anchor = GridBagConstraints.WEST;
		gbc_lblProvincia.insets = new Insets(0, 0, 0, 5);
		gbc_lblProvincia.gridx = 0;
		gbc_lblProvincia.gridy = 7;
		panel.add(lblProvincia, gbc_lblProvincia);
		
		JComboBox comboBox_2 = new JComboBox();
		comboBox_2.setModel(new DefaultComboBoxModel(new String[] {"Huesca", "Zaragoza", "Teruel"}));
		GridBagConstraints gbc_comboBox_2 = new GridBagConstraints();
		gbc_comboBox_2.insets = new Insets(0, 0, 0, 5);
		gbc_comboBox_2.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox_2.gridx = 1;
		gbc_comboBox_2.gridy = 7;
		panel.add(comboBox_2, gbc_comboBox_2);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(SystemColor.controlHighlight);
		tabbedPane.addTab("Datos de facturaci\u00F3n", null, panel_1, null);
		
		JCheckBox chckbxMarcarSoloSi = new JCheckBox("Marcar solo si los datos de facturaci\u00F3n son diferentes de los personales");
		panel_1.add(chckbxMarcarSoloSi);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(SystemColor.controlHighlight);
		tabbedPane.addTab("Datos de Pago", null, panel_2, null);
		
		JRadioButton rdbtnCuentaBancaria = new JRadioButton("Cuenta Bancaria");
		
		JLabel lblIban = new JLabel("IBAN");
		
		JLabel lblEntidad = new JLabel("Entidad");
		
		JLabel lblOficina = new JLabel("Oficina");
		
		JLabel lblDc = new JLabel("DC");
		
		JLabel lblNCuenta = new JLabel("N\u00BA Cuenta");
		
		txtEs = new JTextField();
		txtEs.setBackground(new Color(255, 204, 102));
		txtEs.setText("ES");
		txtEs.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBackground(new Color(255, 204, 102));
		textField_1.setColumns(10);
		
		textField_4 = new JTextField();
		textField_4.setBackground(new Color(255, 204, 102));
		textField_4.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBackground(new Color(255, 204, 102));
		textField_3.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBackground(new Color(255, 204, 102));
		textField_2.setColumns(10);
		
		JRadioButton rdbtnTarjeta = new JRadioButton("Tarjeta");
		
		JLabel lblNTarjeta = new JLabel("N\u00BA tarjeta");
		
		JLabel lblCsv = new JLabel("CSV");
		
		textField_5 = new JTextField();
		textField_5.setBackground(new Color(204, 204, 102));
		textField_5.setColumns(10);
		
		textField_6 = new JTextField();
		textField_6.setBackground(new Color(204, 204, 102));
		textField_6.setColumns(10);
		
		JRadioButton rdbtnTienda = new JRadioButton("Tienda");
		GroupLayout gl_panel_2 = new GroupLayout(panel_2);
		gl_panel_2.setHorizontalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGap(1)
					.addComponent(rdbtnCuentaBancaria)
					.addGap(22)
					.addComponent(lblIban)
					.addGap(28)
					.addComponent(lblEntidad)
					.addGap(16)
					.addComponent(lblOficina)
					.addGap(19)
					.addComponent(lblDc)
					.addGap(18)
					.addComponent(lblNCuenta))
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGap(111)
					.addComponent(txtEs, GroupLayout.PREFERRED_SIZE, 59, GroupLayout.PREFERRED_SIZE)
					.addGap(5)
					.addComponent(textField_1, GroupLayout.PREFERRED_SIZE, 47, GroupLayout.PREFERRED_SIZE)
					.addGap(5)
					.addComponent(textField_4, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
					.addGap(5)
					.addComponent(textField_3, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
					.addGap(5)
					.addComponent(textField_2, GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE))
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGap(1)
					.addComponent(rdbtnTarjeta)
					.addGap(54)
					.addComponent(lblNTarjeta)
					.addGap(117)
					.addComponent(lblCsv))
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGap(111)
					.addComponent(textField_5, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
					.addGap(5)
					.addComponent(textField_6, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGap(1)
					.addComponent(rdbtnTienda))
		);
		gl_panel_2.setVerticalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addComponent(rdbtnCuentaBancaria)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(4)
							.addComponent(lblIban))
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(4)
							.addComponent(lblEntidad))
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(4)
							.addComponent(lblOficina))
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(4)
							.addComponent(lblDc))
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(4)
							.addComponent(lblNCuenta)))
					.addGap(5)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addComponent(txtEs, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(textField_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(textField_4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(textField_3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(textField_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(5)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addComponent(rdbtnTarjeta)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(4)
							.addComponent(lblNTarjeta))
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(4)
							.addComponent(lblCsv)))
					.addGap(5)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addComponent(textField_5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(textField_6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(5)
					.addComponent(rdbtnTienda))
		);
		ButtonGroup group1 = new ButtonGroup();
		group1.add((JRadioButton) rdbtnCuentaBancaria);
	    group1.add((JRadioButton) rdbtnTarjeta);
	    group1.add((JRadioButton) rdbtnTienda);
		panel_2.setLayout(gl_panel_2);
	}

	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
}
