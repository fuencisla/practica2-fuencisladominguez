package libreria;

import java.util.Scanner;

public class TablasMultiplicar {

	public static void main(String[] args) {
		
		Scanner input = new Scanner (System.in);
		
		int opcion;
		
		do {
			System.out.println("\tMEN�");
			System.out.println("1.- Mostrar tabla.");
			System.out.println("2.- Repasar tabla.");
			System.out.println("3.- Repaso global.");
			System.out.println("4.- R�tate m�s all� de las tablas.");
			System.out.println("5.- Salir.");
			opcion = input.nextInt();
			input.nextLine();
			
			switch (opcion) {
			case 1:
				Metodos.mostrarTabla(input);
				break;
			case 2:
				Metodos.repasarTabla(input);
				break;
			case 3:
				Metodos.repasoGeneral(input);
				break;
			case 4:
				Metodos.retoFinal(input);
				break;
			case 5:
				System.out.println("Otro d�a m�s.");
				break;
			default:
				System.out.println("Opci�n incorrecta.");
			}
		} while (opcion != 5);
		
		input.close();
	}
}
